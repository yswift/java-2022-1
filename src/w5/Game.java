package w5;

public class Game {
	// 待猜测的目标数
	private int target;

	// 正在游戏的玩家
	private Player player;

	public void generateTarget() {
		target = (int) (Math.random() * 100);
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void start() {
		generateTarget();
		// 没猜中，游戏继续
		while (!player.isSuccess()) {
			int n = player.guess();
			// 判断猜测结果，并告知玩家
			if (n == target) {
				player.setResult(0);
			} else if (n > target) {
				player.setResult(1);
			} else {
				player.setResult(-1);
			}
		}
	}

}
