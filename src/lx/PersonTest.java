package lx;

public class PersonTest {
	public static void main(String[] args) {
		Person p = new Person();
		p.setName("张三");
		p.setAge(20);
		System.out.println("p 姓名=" + p.getName() + ", 年龄=" + p.getAge());
		
		Person p2 = new Person("李四", 30);
		System.out.println("p2 姓名=" + p2.getName() + ", 年龄=" + p2.getAge());
	}
	

}
