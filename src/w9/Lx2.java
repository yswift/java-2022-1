package w9;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Lx2 {
	public static void main(String[] args) {
		LocalDateTime now = LocalDateTime.now();
		
//		2021-11-5 10:35
		DateTimeFormatter f1 = DateTimeFormatter
				.ofPattern("yyyy-MM-d HH:mm");
		System.out.println(now.format(f1));
		
//		2021-11-5 10:35:10
//
//		2021年11月5日
		DateTimeFormatter f3 = DateTimeFormatter
				.ofPattern("yyyy年MM月d日");
		System.out.println(now.format(f3));
		
//		2021年11月05日
//		2021年11月5日 周五
		DateTimeFormatter f5 = DateTimeFormatter
				.ofPattern("yyyy年MM月d日 EEEE");
		System.out.println(now.format(f5));

		// 2021年11月5日 星期五

	}

}
