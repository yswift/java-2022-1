package w3;

public class Lt_Dog {
	static void cry(double m, int n) {
		System.out.println("小狗");
	}
	
	static void cry(int m, double n) {
		System.out.println("small dog");
	}
	
	public static void main(String[] args) {
		cry(10.0, 10);
		cry(10, 10.0);
		cry(10F, 10);
	}
}
