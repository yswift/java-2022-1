package w7;

public class InstancofDemo {
	public static void main(String[] args) {
		Animal a = new Dog();

		System.out.println("a instanceof Dog ? " + (a instanceof Dog));
		Dog d = (Dog) a;

		System.out.println("a instanceof Canine ? " + (a instanceof Canine));
		Canine c = (Canine) a;

		System.out.println("a instanceof Wolf ? " + (a instanceof Wolf));
//		 Wolf w = (Wolf) a;

	}

}
