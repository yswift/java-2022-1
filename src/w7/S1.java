package w7;

public class S1 extends F1 {
	S1() {
		super(); // 如果你没写，编译器帮你加上
		System.out.println("S1()");
	}
	
	S1(String n) {
		System.out.println("S1(String n) ");
	}

}
