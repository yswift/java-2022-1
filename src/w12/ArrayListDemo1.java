package w12;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo1 {
	public static void main(String[] args) {
//		创建ArrayList<String>对象
//		放入：刘备、关羽、张飞、诸葛亮、赵云、法正、魏延
		ArrayList<String> sg = new ArrayList<>();
		sg.add("刘备");
		sg.add("关羽");
		sg.add("张飞");
		sg.add("诸葛亮");
		sg.add("赵云");
		sg.add("法正");
		sg.add("魏延");
		
		System.out.println("添加完成");
		System.out.println(sg);
		
//		使用ArrayList方法完成:
//		取第4个元素
		String e4 = sg.get(4);
		System.out.println("\n第4个元素：" + e4);
		
//		遍历，并输出元素（分别使用for，和Iterator）
		System.out.println("\nfor 1 遍历");
		for (String e : sg) {
			System.out.println(e);
		}
		System.out.println("\nfor 2 遍历");
		for (int i=0; i<sg.size(); i++) {
			String e = sg.get(i);
			System.out.println(e);
		}
		System.out.println("\nIterator 遍历");
		Iterator<String> it = sg.iterator();
		while (it.hasNext()) {
			String e = it.next();
			System.out.println(e);
		}
//		查找“张飞”，“黄忠”，比较返回值
		int iZf = sg.indexOf("张飞");
		System.out.println("\n张飞的位置：" + iZf);
		int iHz = sg.indexOf("黄忠");
		System.out.println("黄忠的位置：" + iHz);
		
//		判断是否包含： “张飞”，“黄忠”
		boolean bZf = sg.contains("张飞");
		System.out.println("\n包括张飞: " + bZf);
		boolean bHz = sg.contains("黄忠飞");
		System.out.println("包括黄忠: " + bHz);
		
//		删除“诸葛亮”
		boolean success = sg.remove("诸葛亮");
		System.out.println("\n删除“诸葛亮”, success=" + success);
		System.out.println(sg);
		
//		删除第3个元素
		String de = sg.remove(3);
		System.out.println("\n删除删除第3个元素：" + de);
		System.out.println(sg);

	}

}

