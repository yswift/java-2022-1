package w3;

import java.util.Scanner;

public class Lt3 {
	static void printGrade(int score) {
		if (score >= 90) {
			System.out.println("A");
		} else if (score >= 80) {
			System.out.println("B");
		} else if (score >= 70) {
			System.out.println("C");
		} else if (score >= 60) {
			System.out.println("D");
		} else {
			System.out.println("E");
		} 
	}
	
	static String getGrade(int score) {
		if (score >= 90) {
			return "A";
		} else if (score >= 80) {
			return "B";
		} else if (score >= 70) {
			return "C";
		} else if (score >= 60) {
			return "D";
		} else {
			return "E";
		} 
	}

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.print("输入成绩（0-100）:");
		
		int score = reader.nextInt();
		
		printGrade(score);
			
		String r = getGrade(score);
		System.out.println(score + "的等级是" + r);
	}
}
