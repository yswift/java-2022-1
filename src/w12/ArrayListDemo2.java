package w12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArrayListDemo2 {
//	2、用0-100之间的随机数给ArrayList添加元素
	static void init(ArrayList<Integer> a, int n) {
		for (int i = 0; i < n; i++) {
			int v = (int) (Math.random() * 100);
			a.add(v);
		}
	}


//	3、遍历ArrayList元素（从0开始访问ArrayList的每个元素），并输出
	static void print(ArrayList<Integer> a) {
		for (int i = 0; i < a.size(); i++) {
			System.out.print(a.get(i) + " ");
		}
		System.out.println();
	}

//	4、求ArrayList元素的和
	static int sum(ArrayList<Integer> a) {
		int sum = 0;
		for (int i = 0; i < a.size(); i++) {
			sum += a.get(i);
		}
		return sum;
	}

//	5、求ArrayList元素的最大值
	static int max(ArrayList<Integer> a) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i) > max) {
				max = a.get(i);
			}
		}
		return max;
	}
	
//	6、在ArrayList中查找某个元素，找到返回元素的下标，没找到返回-1
	static int find(ArrayList<Integer> a, int value) {
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i) == value) {
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
//		1、定义并创建10个元素的int型ArrayList		
		ArrayList<Integer> a = new ArrayList<>();
		
//		2、用0-100之间的随机数给ArrayList元素赋值
		System.out.println();
		System.out.println("2、用0-100之间的随机数给ArrayList元素赋值");
		init(a, 10);
		
//		3、遍历ArrayList元素（从0开始访问ArrayList的每个元素），并输出
		System.out.println();
		System.out.println("3、遍历ArrayList元素（从0开始访问ArrayList的每个元素），并输出");
		print(a);
		
//		4、求ArrayList元素的和
		System.out.println();
		System.out.println("4、求ArrayList元素的和");
		int sum = sum(a);
		System.out.println("sum = " + sum);
		
//		5、求ArrayList元素的最大值
		System.out.println();
		System.out.println("5、求ArrayList元素的最大值");
		int max = max(a);
		System.out.println("max = " + max);
		
//		6、在ArrayList中查找某个元素，找到返回元素的下标，没找到返回-1
		System.out.println();
		System.out.println("6、在ArrayList中查找某个元素，找到返回元素的下标，没找到返回-1");
		System.out.println("查找：200");
		int idx = find(a, 200);
		System.out.println("结果：" + idx);
		idx = a.indexOf(200);
		System.out.println("使用indexOf结果：" + idx);
		System.out.println("查找：" + a.get(5));
		idx = find(a, a.get(5));
		System.out.println("结果：" + idx);
		idx = a.indexOf(a.get(5));
		System.out.println("使用indexOf结果：" + idx);
		
//		7、使用Arrays.sort 方法排序ArrayList元素
		System.out.println();
		System.out.println("7、使用Arrays.sort 方法排序ArrayList元素");
		System.out.println("结果：");
		Collections.sort(a);
		print(a);
	}
}
