package sy.sy10_2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

// ==== 从这里编写你的代码
class Test {
	// 1、请把下面求数组最小值的方法补充完整，
	static int min(int[] a) {
		// 方法求给定数组a中的最小值

	}
	
	// 2、请把下面求数组平均值的方法补充完整，
	static double avg(int[] a) {
		// 方法求给定数组a中的平均值

	}
	
	// 2、请把下面求数组中位数的方法补充完整，
	// 传入的数组a是已排序过的数组
	// 中位数：https://baike.baidu.com/item/%E4%B8%AD%E4%BD%8D%E6%95%B0/3087401?fr=aladdin
	static double mid(int[] a) {
		// 方法求给定数组a中的中位数

	}
}
// === 代码编写结束

// ==== 以下代码请勿修改
public class Main {
	/**
	 * 生成数组
	 * @param n 数组元素个数
	 * @param seed 随机数种子
	 * @return 初始化好的数组
	 */
	static int[] init(int n, int seed) {
		int[] a = new int[n];
		// seed 初始化随机数生成器
		Random r = new Random(seed);
		for (int i=0; i < n; i++) {
			a[i] = r.nextInt(100);
		}
		return a;
	}
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// 从键盘输入数组元素和随机数种子
		int n = reader.nextInt();
		int seed = reader.nextInt();
		
		// 建立数组
		int[] a = init(n, seed);
		// 输出数组
		System.out.println("数组：");
		System.out.println(Arrays.toString(a));
		
		// 计算最小值
		int min = Test.min(a);
		System.out.println("最小值是：" + min);
		
		// 计算平均值
		double avg = Test.avg(a);
		System.out.printf("平均值是：%.2f\n", avg);
		
		// 计算中位数
		System.out.println("计算中位数");
		// 先排序
		Arrays.sort(a);
		System.out.println(Arrays.toString(a));
		// 再计算
		double mid = Test.mid(a);
		System.out.printf("中位数是：%.2f\n", mid);
				
	}

}
