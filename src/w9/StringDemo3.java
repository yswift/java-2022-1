package w9;

public class StringDemo3 {
	public static void main(String[] args) {
		String line = "123+10=133";
		int idxOp = line.indexOf("+");
		int idxEq = line.indexOf("=");
		
		String add1 = line.substring(0, idxOp);
		String add2 = line.substring(idxOp+1, idxEq);
		String res = line.substring(idxEq+1, line.length());
		System.out.println("add1 = " + add1);
		System.out.println("add2 = " + add2);
		System.out.println("res = " + res);
	}

}
