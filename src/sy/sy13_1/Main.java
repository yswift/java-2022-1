package sy.sy13_1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) throws IOException {
		// 读取随机数种子
		Scanner reader = new Scanner(System.in);
		int seed = reader.nextInt();
		Random random = new Random(seed);

		// 生成10个100以内的随机数并存入 ArrayList<Integer> 中
		List<Integer> list = Stream.generate(() -> random.nextInt(100)).limit(10).collect(Collectors.toList());

		// 建立流 ByteArrayOutputStream
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		// 包装成 ObjectOutputStream，因为这个类才有写入int值的方法
		try (ObjectOutputStream oos = new ObjectOutputStream(bos)) {
			// 写数据
			Test.write(oos, list);
		}

		// 存放读取数据的 ArrayList
		ArrayList<Integer> list2 = new ArrayList<>();
		
		// 获取写入的数据
		byte[] bytes = bos.toByteArray();
		// 建立流 ByteArrayInputStream
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		// 包装成 ObjectInputStream，因为这个类才有读取int值的方法
		try (ObjectInputStream ois = new ObjectInputStream(bis)) {
			// 读数据
			Test.read(ois, list2);
		}

		// 输出
		System.out.println(list2);
	}

}

//==== 从这里开始编写你的程序
class Test {
	/**
	 * 编写方法，把 list 中的偶数，写入到 writeInt 流中
	 * 
	 * 提示: 遍历 list，调用 DataOutputStream 类的 writeInt 方法
	 * 
	 * @param dos  输出流
	 * @param list 整数list
	 * @throws IOException
	 */
	public static void write(ObjectOutputStream oos, List<Integer> list) throws IOException {

	}

	/**
	 * 编写方法，从 ObjectInputStream ois 中读取整数，并加入到 List<Integer> list 中
	 * 
	 * 提示：用 ObjectInputStream 的 available() 方法判断是否还有可以读取的数据，
	 * 用 readInt() 读取数据
	 * 
	 * @param ois 输入流
	 * @param list 保存整数的list
	 * @throws IOException
	 */
	public static void read(ObjectInputStream ois, List<Integer> list) throws IOException {

	}

}
//==== 编写结束