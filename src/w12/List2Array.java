package w12;

import java.util.ArrayList;
import java.util.Arrays;

public class List2Array {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("刘备");
		list.add("关羽");
		list.add("张飞");
		list.add("诸葛亮");
		list.add("赵云");
		list.add("法正");
		list.add("魏延");
		System.out.println("List: ");
		System.out.println(list);
		System.out.println();
		
		
		String[] array=list.toArray(new String[list.size()]);
		System.out.println("Array1: ");
		System.out.println(array);
		System.out.println("Array2: ");
		System.out.println(Arrays.toString(array));
		
		ArrayList<Integer> listInt = new ArrayList<>();
		listInt.add(100);
		listInt.add(200);
		Integer[] a1 = listInt.toArray(new Integer[0]);

	}

}
