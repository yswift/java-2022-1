package b1;

public abstract class Animal {
	String picture;
	String food;
	String hunger;
	String boundaries;
	String location;
	
	public abstract void makeNoise();
	
	public abstract  void eat();

	public void sleep() {
		System.out.println("animal sleep");
	}
	
	public void roam() {
		System.out.println("animal roam");
	}
}
