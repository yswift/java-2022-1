package w3;

public class Lt1 {
	public static int max(int num1, int num2) {
		int result;
		if (num1 > num2) {
			result  = num1;
		} else {
			result = num2;
		}
		return num2;
	}

	public static void main(String[] args) {
		int i1=10, i2 = 50;
		int m = max(i1, i2);
		System.out.println("max = " + m);
	}
}
