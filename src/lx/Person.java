package lx;

public class Person {
//	有两个实例变量
//	姓名
	private String name;
//	年龄
	private int age = 10;

	
//
//	有默认构造方法、
	public Person() {
		
	}
	
//	有姓名，年龄的构造方法
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
//
//	为每个实例变量都封装
	// getter//setter 
	// 1)类型
	// 2)命名规则
	// 3)参数，返回
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
}
