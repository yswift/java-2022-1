package w9;

public class MathLx {
	public static void main(String[] args) {
//		使用Math类的常用方法完成下面的练习。  
		double a = 5;
		double b = 7;
		
//		1)	求a,b两个数中较大的一个；
		double max = Math.max(a, b);
		System.out.println("max = " + max);
		
//		2)	求a的平方根；
		double sa = Math.sqrt(a);
		System.out.println("sqrt(a) = " + sa);
		
//		3)	求a的b次方；
		double pa = Math.pow(a, b);
		System.out.println("a^b = " + pa);
		
//		4)	给定一个数组double[] nums = {3.4, 2.5, 1.6, -3.4, -2.5, -1.6},
//		要求用这个数组中的每个元素做参数，分别调用Math类中floor、round 
//		和ceil这三个方法，并体会这3个方法的区别。
		double[] nums = {3.4, 2.5, 1.6, -3.4, -2.5, -1.6};
		for (double n : nums) {
			System.out.println("n = " + n);
			System.out.println("floor(n) = " + Math.floor(n));
			System.out.println("round(n) = " + Math.round(n));
			System.out.println("ceil(n) = " + Math.ceil(n));
			System.out.println();
		}

	}

}
