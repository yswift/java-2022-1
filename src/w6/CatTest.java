package w6;

public class CatTest {
	public static void main(String[] args) {
		Animal c = new Cat();
		c.eat();
		c.makeNoise();
		c.sleep();
		c.roam();
	}

}
