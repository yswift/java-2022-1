package w9;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegDemo2 {
	public static void main(String[] args) {
		String id = "532522200102030011";
		
		Pattern p = Pattern.compile("\\d{6}(\\d{8})\\d{3}[0-9xX]");
		Matcher m = p.matcher(id);
		if (m.matches()) {
			System.out.println("正确的身份证号码");
			System.out.println("出生日期:" + m.group(1));
		} else {
			System.out.println("不正确的身份证号");
		}
	}

}
