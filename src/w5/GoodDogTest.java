package w5;

public class GoodDogTest {
	public static void main(String[] args) {
//		Dog d = new Dog();
//		d.size = 0;
//		d.bark();
		
		GoodDog gd = new GoodDog();
//		gd.size = 0;
		// gd.size = 0; 不能直接赋值
		// 赋值 调用 setter
//		gd.setSize(0);
		gd.setSize(50);
//		gd.setSize(200);
		gd.bark();
//		System.out.println("gd.size = " + gd.size);
		// 取值调用 getter
		System.out.println("gd.size = " + gd.getSize());
	}

}
