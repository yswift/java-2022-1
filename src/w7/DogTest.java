package w7;

public class DogTest {
	public static void main(String[] args) {
//		Animal a = new Animal(); // 抽象类不能用来创建对象
		Animal a = new Dog();
		
		a.makeNoise();
		a.eat();
		
		a = new Wolf();
		a.makeNoise();
		
		Pet p = new Dog();
		p.beFriendly();
		p.play();
//		p.makeNoise();
		
	}

}
