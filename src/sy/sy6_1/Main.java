package sy.sy6_1;

import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Supplier;

// ==== 从这里开始编写你的代码
// 1. 创建动物类，其叫声为输出“叽里咕噜” （代码已写好）
class Animal {
    public void shout() {
        System.out.println("叽里咕噜");
    }
}

// 2. 猫类继承动物类，发出叫声“喵喵”, 要求：类名：Cat， Override(覆盖，改写，重写)父类中的 shout 方法
class Cat extends Animal {
	//  Override(覆盖，改写，重写)父类中的 shout 方法

}

// 3. 狗类继承动物类，发出叫声“汪汪”，要求：类名：Dog， Override(覆盖，改写，重写)父类中的 shout 方法


// 4. 鸭类继承动物类，发出叫声“咯咯”，要求：类名：Duck， Override(覆盖，改写，重写)父类中的 shout 方法


// 5. 鸡类继承动物类，发出叫声“咯咯”，要求：类名：Chicken， Override(覆盖，改写，重写)父类中的 shout 方法


// ==== 编写代码结束

// === 请勿修改下面的代码
public class Main {
    private static HashMap<String, Supplier<Animal>> name2Creator = new HashMap<>();
    static {
        name2Creator.put("animal", () -> new Animal());
        name2Creator.put("cat", () -> new Cat());
        name2Creator.put("dog", () -> new Dog());
        name2Creator.put("duck", () -> new Duck());
        name2Creator.put("chicken", () -> new Chicken());
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String name = reader.next();
        if (name == null || name.isEmpty()) {
          	return;
        }
        Supplier<Animal> supplier = name2Creator.get(name.toLowerCase());
        if (supplier == null) {
            System.out.println("未知动物：" + name);
            return;
        }
        Animal a = supplier.get();
        System.out.print(name + " : ");
        a.shout();
    }
}
