package sy.sy4_1;
// 实验4-1的模板，复制到网页后删除上一行: package sy...;

import java.util.Scanner;

//==== 从这里开始写你的代码
class Rect {
	// === 两个实例变量：
	// 矩形的宽width，double类型
	// 矩形的高height，double类型

	// 构造方法：一个带有两个参数的构造方法，用于将width和height属性初化；
	public Rect(double width, double height) {
	}

	// 求矩形面积的方法 public double area() 返回面积
	public double area() {
	}

	// 求矩形周长的方法 public double perimeter() 返回周长

}
//==== 你的代码编写结束

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		double w = in.nextDouble();
		double h = in.nextDouble();
		Rect r = new Rect(w, h);
		System.out.printf("%.2f\n", r.area());
		System.out.printf("%.2f\n", r.perimeter());
	}
}
