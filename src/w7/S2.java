package w7;

public class S2 extends F2 {
	S2() {
		// 编译器自动加 super(), 
		// 但不会自动加 super("s2");
		super("s2");
	}
	
	S2(String s) {
//		super("123");
		this();
	}

}
