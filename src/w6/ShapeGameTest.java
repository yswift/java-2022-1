package w6;

public class ShapeGameTest {
	public static void main(String[] args) {
		ShapeGame shapeGame = new ShapeGame();
		
		System.out.println("处理Square");
		Square s = new Square();
		shapeGame.down(s);
		
		System.out.println("处理Amoeba");
		Amoeba a = new Amoeba();
		shapeGame.down(a);
		
		System.out.println("\n处理Circle");
		Circle c = new Circle();
		shapeGame.down(c);
		
	}

}
