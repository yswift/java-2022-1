package w3;

public class Lt4 {
	/* 
	 * 判断n是否是素数，是返回true, 不是返回false
	 */
	static boolean isPrime(int n) {
		if (n < 2) {
			return false;
		}
		if (n == 2) {
			return true;
		}
		for (int i=2; i<=n/2; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
		
	}

	public static void main(String[] args) {
		// 输出100内的素数
		for (int i=2; i<100; i++) {
			if (isPrime(i)) {
				System.out.println(i);
			}
		}
		// 验证哥德巴赫猜想
		// 大于等于6的偶数，可以分解成两个素数的和
		for (int i = 6; i<100; i+=2) {
			for (int j=2; j<i; j++) {
				if (isPrime(j) && isPrime(i-j)) {
					System.out.println(i + " = " + j + " + " + (i-j));
					break;
				}
			}
		}
	}
}
