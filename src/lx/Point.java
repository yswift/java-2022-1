package lx;

public class Point {
//	1、两个表示横坐标，纵坐标的实例变量 x, y (都是double类型)
	// ctrl+/
	private double x;
	private double y;

//	2、两个构造方法：

//		1）默认的无参数的构造方法
	public Point() {
		
	}

		2）以横坐标，纵坐标为参数的构造方法

	3、横坐标 x 的getter和setter

	4、纵坐标 y 的getter和setter

	5、求两点间距离的方法 public double distance(Point p)
}
