package w6;

public class ShapeTest2 {
	public static void main(String[] args) {
		Shape s;
		
		System.out.println("square");
		s = new Square();
		s.rotate();
		s.playSound();
		
		System.out.println("amoeba");
		s = new Amoeba();
		s.rotate();
		s.playSound();
	}
}
