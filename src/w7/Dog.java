package w7;

public class Dog extends Canine  implements Pet  {

	@Override
	public void play() {
		System.out.println("dog play");
	}

	@Override
	public void beFriendly() {
		System.out.println("dog beFriendly");
	}

//	@Override
	public void makeNoise() {
		// TODO Auto-generated method stub
		System.out.println("dog makeNoise");
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("dog eat");
	}

}
