package w6;

public class ShapeTest {
	public static void main(String[] args) {
		System.out.println("square");
		Square s = new Square();
		s.rotate();
		s.playSound();
		
		System.out.println("\ntriangle");
		Triangle t = new Triangle();
		t.rotate();
		t.playSound();
		
		System.out.println("\namoeba");
		Amoeba a = new Amoeba();
		a.rotate();
		a.playSound();
	}
	

}
