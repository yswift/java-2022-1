package w9;

public class StringBuilderDemo {
	// 把一个数组中的元素合成字符串，用“，”分隔
	public static void main(String[] args) {
		int[] a = new int[10];
		for (int i=0; i<a.length; i++) {
			a[i] = (int)(Math.random()*100);
		}
		
		StringBuilder sb = new StringBuilder();
		for (int v:a) {
			sb.append(v).append(", ");
		}
		sb.delete(sb.length()-2, sb.length());
		String s = sb.toString();
		
		System.out.println(s);
	}

}
