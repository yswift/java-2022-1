package w15_2;

import w6.Animal;
import w6.Cat;
import w6.Dog;

public class Demo2 {
	public static void main(String[] args) {
		Animal a = new Dog();
		a.makeNoise();
		a.eat();
		a.roam();
		System.out.println();
		
		a = new Cat();
		a.makeNoise();
		a.eat();
		a.roam();
	}

}
