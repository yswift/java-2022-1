package w7;

public abstract class Animal {
	String picture;
	String food;
	String hunger;
	String boundaries;
	String location;
	
	public abstract void makeNoise();
//	public  void makeNoise() {
//		System.out.println("animal make noise");
//	}
	
	public void eat() {
		System.out.println("animal eat");
	}

	public void sleep() {
		System.out.println("animal sleep");
	}
	
	public void roam() {
		System.out.println("animal roam");
	}
}
