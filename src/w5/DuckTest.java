package w5;

public class DuckTest {
	public static void main(String[] args) {
		Duck d1 = new Duck(50);
		Duck d2 = new Duck();
		
		d1.size = -10;
		System.out.println("d1.size = " + d1.size);
		System.out.println("d2.size = " + d2.size);
	}

}
