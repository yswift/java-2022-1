package w5;

public class GoodDog {
	private int size;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		if (size < 1 || size > 200) {
			throw new IllegalArgumentException("非法的size = " + size + ", size 必须在1-199之间");
		}
		this.size = size;
	}

	void bark() {
		if (size > 60) {
			System.out.println("Wooof! Wooof!");
		} else if (size > 14) {
			System.out.println("Ruff! Ruff!");
		} else {
			System.out.println("Yip! Yip!");
		}
	}

}
