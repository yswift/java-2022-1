package w9;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Lx4 {
	public static void main(String[] args) {
		LocalDate d1 = LocalDate.of(2021, 8, 27);
		LocalTime t1 = LocalTime.of(16, 10);
		LocalDateTime dt1 = LocalDateTime.of(d1, t1);
		LocalDateTime dt2 = LocalDateTime.of(2021, 11, 5, 16, 11);
		System.out.println(d1);
		System.out.println(t1);
		System.out.println(dt1);
		System.out.println(dt2);
		
	}

}
