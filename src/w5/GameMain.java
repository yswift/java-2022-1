package w5;

public class GameMain {
	public static void main(String[] args) {
		// 创建游戏
		Game game = new Game();
		// 准备待猜测目标
		game.generateTarget();
		// 加入玩家
		Player p1 = new Player("张三");
		game.setPlayer(p1);
		
		// 启动游戏
		game.start();
	}

}
