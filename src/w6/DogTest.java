package w6;

public class DogTest {
	public static void main(String[] args) {
		Dog d = new Dog();
		
		d.beFriendly();
		d.play();
		d.makeNoise();
		d.eat();
		d.sleep();
		d.roam();
		
		System.out.println("\nanmial ");
		Animal a = d;
//		a.beFriendly();
//		a.play();
		a.makeNoise();
		a.eat();
		a.sleep();
		a.roam();
		
		System.out.println("\nPet");
		Pet p = d;
		p.beFriendly();
		p.play();
//		p.makeNoise();
//		p.eat();
//		p.sleep();
//		p.roam();
	}

}
