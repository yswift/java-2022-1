package b1;

public class Pigeon extends Birds implements Pet {
	@Override
	public void fly() {
		System.out.println("Pigeon fly");
	}

	@Override
	public void makeNoise() {
		System.out.println("Pigeon make noise");
	}

	@Override
	public void eat() {
		System.out.println("Pigeon eat");
	}

	@Override
	public void play() {
		System.out.println("Pigeon play");
	}

	@Override
	public void beFriendly() {
		System.out.println("Pigeon Fridendly");
	}

}
