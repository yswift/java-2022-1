package w5;

/**
 * 
 * @author yswif
 *
 */
public class Duck {
	int size;
	
	public Duck() {
		// this 调用另外的构造函数
		this(27); // => this.size = 27;
	}
//	
	public Duck(int size) {
		// 对象本身的引用, 一般在局部变量和实例变量同名时使用
		this.size = size;
	}

	public int getSize() {
		return size;
	}
}
