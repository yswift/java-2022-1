package w5;

import java.util.Scanner;

public class Player {
	protected final String nickname;
	
	// 默认第一次猜测的数是0
	protected int guessNumber = 0;
	
	// 默认第一次猜测结果是过小
	// 猜测结果 0：猜中，1：过大，-1：过小
	protected int result = -1;
	
	private final Scanner input = new Scanner(System.in);
	
	public Player(String nickname) {
		this.nickname = nickname;
	}

	public int guess() {
		System.out.print(nickname + ",请输入0-100之间的数：");
		guessNumber = input.nextInt();
		return guessNumber;
	}
	
	public String getNickname() {
		return nickname;
	}

	public void setResult(int result) {
		this.result = result;
		System.out.print(nickname + ",猜:" + guessNumber);
		if (result > 0) {
			System.out.println("过大");
		} else if (result < 0) {
			System.out.println("过小");
		} else {
			System.out.println("猜中");
		}
	}
	
	public boolean isSuccess() {
		return result == 0;
	}
	
}
