package w5;

public class StaticDemo1 {
	int a;
	static int b;
	
	static void f1() {
		// a = 10;
		b = 20;
		System.out.println("静态方法f1()");
	}
	
	void f2() {
		a = 10;
		b = 20;
		System.out.println("非静态方法分（）");
	}
	
	public static void main(String[] args) {
		f1();
		// 在静态方法main中不能直接调用非静态方法 f2()
		// f2();
		// 但可以通过创建对象来调用
		StaticDemo1 sd1 = new StaticDemo1();
		sd1.f2();
	}

}
