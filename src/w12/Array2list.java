package w12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Array2list {
	public static void main(String[] args) {
		String[] array = {"java", "c"};
		List<String> list = new ArrayList<String>(Arrays.asList(array));
		System.out.println("list: ");
		System.out.println(list);
		System.out.println();
		System.out.println("删除元素：java");
		list.remove("java");
		System.out.println(list);
		System.out.println();
		
		
		List<String> list2 = Arrays.asList(array);
		System.out.println("List2: ");
		System.out.println(list2);
		System.out.println();
		System.out.println("删除元素：c");
		list2.remove("c");
		System.out.println(list2);
		System.out.println();
	}

}
