package sy.sy11_1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;


public class Main {
	/**
	 * 生成ArrayList
	 * @param n ArrayList元素个数
	 * @param seed 随机数种子
	 * @return 初始化好的ArrayList
	 */
	static ArrayList<Integer> init(int n, int seed) {
		ArrayList<Integer> a = new ArrayList<>();
		// seed 初始化随机数生成器
		Random r = new Random(seed);
		for (int i=0; i < n; i++) {
			int v = r.nextInt(100);
			a.add(v);
		}
		return a;
	}
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// 从键盘输入ArrayList元素和随机数种子
		int n = reader.nextInt();
		int seed = reader.nextInt();
		
		// 建立ArrayList
		ArrayList<Integer> a = init(n, seed);
		// 输出ArrayList
		System.out.println("ArrayList：");
		System.out.println(a);
		
		// 计算最小值
		int min = Test.min(a);
		System.out.println("最小值是：" + min);
		
		// 计算平均值
		double avg = Test.avg(a);
		System.out.printf("平均值是：%.2f\n", avg);
		
		// 计算中位数
		System.out.println("计算中位数");
		// 先排序
		Collections.sort(a);
		System.out.println(a);
		// 再计算
		double mid = Test.mid(a);
		System.out.printf("中位数是：%.2f\n", mid);
				
	}

}
//==== 以上代码请勿修改

//==== 从这里编写你的代码
class Test {
	// 1、请把下面求ArrayList最小值的方法补充完整，
	static int min(ArrayList<Integer> a) {
		// 方法求给定ArrayLista中的最小值
	}
	
	// 2、请把下面求ArrayList平均值的方法补充完整，
	static double avg(ArrayList<Integer> a) {
		// 方法求给定ArrayLista中的平均值
	}
	
	// 3、请把下面求ArrayList中位数的方法补充完整，
	// 传入的ArrayLista是已排序过的ArrayList
	// 中位数：https://baike.baidu.com/item/%E4%B8%AD%E4%BD%8D%E6%95%B0/3087401?fr=aladdin
	static double mid(ArrayList<Integer> a) {
		// 方法求给定ArrayLista中的中位数
	}
}
//=== 代码编写结束