package w9;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringDemo4 {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("(\\d+)\\s*\\+\\s*(\\d+)\\s*=\\s*(\\d+)");
		String line = "123    + 1550=   133";
		Matcher m = p.matcher(line);
		
		if (m.matches()) {
			String add1 = m.group(1);
			String add2 = m.group(2);
			String res = m.group(3);
			
			System.out.println("add1 = " + add1);
			System.out.println("add2 = " + add2);
			System.out.println("res = " + res);
		}
	}
}
