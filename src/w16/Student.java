package w16;

import java.time.LocalDate;

public class Student {
	private String no;
	private String name;
	private int age;
	private LocalDate birthday;
	
	
	
	public Student() {
		
	}
	
	
	public Student(String no, String name) {
		super();
		this.no = no;
		this.name = name;
	}


	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}


	@Override
	public String toString() {
		return "Student [no=" + no + ", name=" + name + ", age=" + age + ", birthday=" + birthday + "]";
	}

	
}
