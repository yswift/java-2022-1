package w6;

public class Amoeba extends Shape {
	@Override
	public void rotate() {
		System.out.println("amoeba rotate");
	}
	
	public void playSound() {
		System.out.println("amoeba play sound");
	}
}
