package b1;

public class USKTest {
	public static void main(String[] args) {
        //生成一个实现可USB接口（标准）的U盘对象
        USBDisk u = new USBDisk();
        //调用U盘的read( )方法读取数据
        u.read();
        //调用U盘的write( )方法写入数据
        u.write();
        
        //生成一个实现可USB接口（标准）的键盘对象
        USBKeyboard k = new USBKeyboard();
        //调用键盘的read( )方法读取数据
        k.read();
        //调用键盘的write( )方法写入数据
        k.write();
    }
}
