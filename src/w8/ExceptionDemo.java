package w8;

public class ExceptionDemo {
	static void f(boolean haveException) throws Exception {
		System.out.println("start f()");
		if (haveException) {
			throw new Exception("有异常");
		}
		System.out.println("end f()");
	}
	
	static void f2(boolean haveException) throws Exception {
		f(haveException);
	}

	public static void main(String[] args) {
		System.out.println("start main");
		try {
			System.out.println("start try");
			f(false);
			System.out.println("end try");
		} catch (Exception e) {
			System.out.println("catch excption");
		}
		System.out.println("end main");
	}
}
